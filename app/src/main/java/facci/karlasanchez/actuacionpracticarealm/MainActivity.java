package facci.karlasanchez.actuacionpracticarealm;

import androidx.appcompat.app.AppCompatActivity;
import io.realm.Realm;
import io.realm.mongodb.App;
import io.realm.mongodb.AppConfiguration;
import io.realm.mongodb.Credentials;
import io.realm.mongodb.User;
import io.realm.mongodb.mongo.MongoClient;
import io.realm.mongodb.mongo.MongoCollection;
import io.realm.mongodb.mongo.MongoDatabase;

import org.bson.Document;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    String App_ID = "application-0-mlyzk";

    private App app;
    private EditText dataEditText;
    private Button button;

    MongoDatabase mongoDatabase;
    MongoClient mongoClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button = (Button) findViewById(R.id.button);
        dataEditText = (EditText) findViewById(R.id.data);


        Realm.init(this);

        app = new App(new AppConfiguration.Builder(App_ID).build());



        Credentials credentials = Credentials.emailPassword("melmusica04@gmail.com", "Karlamel23");


        app.loginAsync(credentials, new App.Callback<User>() {
            @Override
            public void onResult(App.Result<User> result) {
                if(result.isSuccess()){
                    Log.v("User", "Usuario: Conexion Correcta");
                }else{
                    Log.v("User", "Usuario: Conexion Fallida");
                }
            }
        });

        button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                User user = app.currentUser();
                mongoClient = user.getMongoClient("mongodb-atlas");
                mongoDatabase = mongoClient.getDatabase("PracticaRealm");
                MongoCollection<Document> mongoCollection = mongoDatabase.getCollection("TestData");

                mongoCollection.insertOne(new Document ("userid",user.getId()).append("data",dataEditText.getText().toString())).getAsync(result -> {
                    if (result.isSuccess()){
                        Log.v("Data", "Datos Insertados correctamente");
                    }else{
                        Log.v("Data", "Error: " + result.getError().toString());
                    }
                });
            }
        });
    }
}